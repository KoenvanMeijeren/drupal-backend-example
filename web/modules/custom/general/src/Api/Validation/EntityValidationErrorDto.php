<?php

namespace Drupal\general\Api\Validation;

use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Provides a class for EntityValidationErrorsDto.
 */
final readonly class EntityValidationErrorDto {

  /**
   * DTO for entity validation errors.
   */
  public function __construct(
    public string $message,
    public string $property,
    public string $error,
  ) {}

  /**
   * Create from constraint violation error.
   */
  public static function createFrom(ConstraintViolationInterface $error): self {
    return new self(
      $error->getMessage(),
      $error->getPropertyPath(),
      (string) $error
    );
  }

  /**
   * Convert to array.
   */
  public function toArray(): array {
    return [
      'message' => $this->message,
      'property' => $this->property,
      'error' => $this->error,
    ];
  }

}
