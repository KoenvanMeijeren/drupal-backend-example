<?php

namespace Drupal\general\Api\Validation;

use Drupal\Core\Entity\EntityConstraintViolationListInterface;

/**
 * Provides a class for EntityValidationErrorsDto.
 */
final readonly class EntityValidationErrorItemsDto {

  /**
   * Whether the DTO has errors.
   */
  public readonly bool $hasErrors;

  /**
   * DTO for entity validation errors.
   *
   * @param \Drupal\general\Api\Validation\EntityValidationErrorDto[] $errors
   *   The items.
   */
  public function __construct(
    public array $errors,
  ) {
    $this->hasErrors = count($errors) > 0;
  }

  /**
   * Create from validation errors.
   */
  public static function createFrom(EntityConstraintViolationListInterface $validation_errors): self {
    $errors = [];
    foreach ($validation_errors as $error) {
      $errors[] = EntityValidationErrorDto::createFrom($error);
    }

    return new self($errors);
  }

  /**
   * Convert to array.
   */
  public function toArray(): array {
    $errors = [];
    foreach ($this->errors as $error) {
      $errors[] = $error->toArray();
    }

    return $errors;
  }

}
