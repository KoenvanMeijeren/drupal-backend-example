<?php

namespace Drupal\general\Api\Validation;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a class for ValidationErrorResponse.
 */
final class ValidationErrorResponse extends JsonResponse {

  /**
   * Validation error response.
   */
  public function __construct(EntityValidationErrorItemsDto $errors) {
    parent::__construct([
      'errors' => $errors->toArray(),
    ], self::HTTP_BAD_REQUEST);
  }

}
