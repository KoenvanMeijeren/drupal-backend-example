<?php

namespace Drupal\general\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a class for GeneralController.
 */
final class GeneralController extends ControllerBase {

  /**
   * Returns the authenticated JSON response.
   */
  public function authenticated(): JsonResponse {
    return new JsonResponse([
      'user' => $this->currentUser()->getDisplayName(),
    ]);
  }

  /**
   * Returns the unauthenticated JSON response.
   */
  public function unauthenticated(): JsonResponse {
    return new JsonResponse([
      'user' => $this->currentUser()->getDisplayName(),
    ]);
  }

}
