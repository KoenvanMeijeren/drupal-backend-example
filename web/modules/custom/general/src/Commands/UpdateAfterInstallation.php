<?php

namespace Drupal\general\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;

/**
 * Provides a class for the update config command.
 *
 * @package Drupal\general\Command
 */
final class UpdateAfterInstallation extends DrushCommands {

  /**
   * Constructs a new object.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
  ) {
    parent::__construct();
  }

  /**
   * Updates the config.
   */
  #[CLI\Command(name: 'update-after-install')]
  #[CLI\Usage(name: 'drush update-after-install', description: 'Updates the site after install.')]
  public function execute(): void {
    $config = $this->configFactory->getEditable('system.performance');

    $config->set('css.preprocess', FALSE);
    $config->set('js.preprocess', FALSE);
    $config->save();

    $output = $this->output();
    $output->writeln('<info>Successfully updated the site after installation!</info>');
  }

}
