<?php

namespace Drupal\general\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Requires authentication for all JSON API routes.
 */
final class RequireAuthenticatedApiRoutesSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    foreach ($collection->all() as $route) {
      // Only alter routes that don't have the '_role' requirement. We don't
      // want to override any existing requirements.
      if ($route->hasRequirement('_role')) {
        continue;
      }

      if (str_starts_with($route->getPath(), '/jsonapi/')) {
        $route->setRequirement('_role', 'authenticated');
      }

      if (str_starts_with($route->getPath(), '/api/')) {
        $route->setRequirement('_role', 'authenticated');
        $route->setOption('_auth', ['oauth2']);
      }
    }
  }

}
