# Default content
This module contains all the default content for the Site.

## Users
Users are created in the .install file, instead of exporting them to the
default content. It is not allowed to export the users to the default content.
This is mainly done to be able to set a different password for the users in
local and external environments.

It is also important to note that you need to make sure that the right user
UUID's is used in the other entities which have references to users.

## Export
Entities can be exported to the default content by using the commands below.

### One entity
```
drush default-content:export-references ENTITYTYPE ENTITYID \
--folder=modules/custom/site_default_content/content
```

### All entities
```
drush default-content:export-references consumer \
--folder=modules/custom/site_default_content/content
```

### Known errors
#### Export failed because of file-related errors
These errors usually occur because of missing files or changed files. You can
fix this error by making sure that the files exist on your file system. After
exporting them, you need to make sure that all file entities are correctly
referenced in the (existing) entities.
