<?php

namespace Drupal\task\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a task entity type.
 */
interface TaskInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Sets the values for this entity.
   */
  public function setValues(array $values): void;

  /**
   * Gets the description.
   */
  public function getDescription(): ?string;

  /**
   * Gets the created time.
   */
  public function getCreatedTime(): int;

}
