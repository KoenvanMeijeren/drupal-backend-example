<?php

namespace Drupal\task\Api\Input;

use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a class for TaskCreateInputDto.
 */
final readonly class TaskApiInputDto {

  /**
   * Input DTO for task creation.
   */
  public function __construct(
    public ?string $label,
    public ?string $description,
  ) {}

  /**
   * Create a task from request.
   */
  public static function fromRequest(Request $request): self {
    $data = $request->toArray();

    return new self(
      $data['label'] ?? NULL,
      $data['description'] ?? NULL
    );
  }

  /**
   * Convert to array.
   */
  public function toArray(): array {
    return [
      'label' => $this->label,
      'description' => $this->description,
    ];
  }

}
