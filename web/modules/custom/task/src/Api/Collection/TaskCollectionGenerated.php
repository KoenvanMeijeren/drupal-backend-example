<?php

namespace Drupal\task\Api\Collection;

/**
 * Provides a class for TaskCollection.
 */
final readonly class TaskCollectionGenerated implements \JsonSerializable {

  /**
   * Constructs a new TaskCollection.
   */
  public function __construct(
    public \Generator $tasks,
  ) {}

  /**
   * Create from entities generator.
   */
  private static function fromEntitiesGenerator(array $entities): \Generator {
    foreach ($entities as $entity) {
      yield TaskItemDto::fromArray($entity);
    }
  }

  /**
   * Create from array.
   */
  public static function fromEntities(array $entities): self {
    return new self(self::fromEntitiesGenerator($entities));
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

  /**
   * Convert to array.
   */
  public function toArray(): array {
    $tasks = [];
    /** @var \Drupal\task\Api\Collection\TaskItemDto $task */
    foreach ($this->tasks as $task) {
      $tasks[] = $task->toArray();
    }

    return $tasks;
  }

}
