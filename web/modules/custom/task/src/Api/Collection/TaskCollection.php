<?php

namespace Drupal\task\Api\Collection;

/**
 * Provides a class for TaskCollection.
 */
final readonly class TaskCollection implements \JsonSerializable {

  /**
   * Constructs a new TaskCollection.
   *
   * @param \Drupal\task\Api\Collection\TaskItemDto[] $tasks
   *   The tasks.
   */
  public function __construct(
    public array $tasks,
  ) {}

  /**
   * Create from array.
   */
  public static function fromEntities(array $entities): self {
    $tasks = [];
    foreach ($entities as $entity) {
      $tasks[] = TaskItemDto::fromArray($entity);
    }

    return new self($tasks);
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

  /**
   * Convert to array.
   */
  public function toArray(): array {
    $tasks = [];
    foreach ($this->tasks as $task) {
      $tasks[] = $task->toArray();
    }

    return $tasks;
  }

}
