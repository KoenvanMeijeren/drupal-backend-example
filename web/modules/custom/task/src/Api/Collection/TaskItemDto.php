<?php

namespace Drupal\task\Api\Collection;

use Drupal\task\Entity\TaskInterface;

/**
 * Provides a class for TaskCollection.
 */
final readonly class TaskItemDto implements \JsonSerializable {

  /**
   * Constructs a new TaskItemDto.
   */
  public function __construct(
    public int|string|NULL $id,
    public ?string $uuid,
    public string $label,
    public ?string $description,
    public bool $status,
    public int $created,
    public int $changed,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function fromArray(TaskInterface $task): self {
    return new self(
      $task->id(),
      $task->uuid(),
      (string) $task->label(),
      $task->getDescription(),
      $task->isPublished(),
      $task->getCreatedTime(),
      $task->getChangedTime()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

  /**
   * Convert to array.
   */
  public function toArray(): array {
    return [
      'id' => $this->id,
      'uuid' => $this->uuid,
      'label' => $this->label,
      'description' => $this->description,
      'status' => $this->status,
      'created' => $this->created,
      'changed' => $this->changed,
    ];
  }

}
