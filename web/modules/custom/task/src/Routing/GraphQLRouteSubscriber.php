<?php

namespace Drupal\task\Routing;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\graphql_extra_cache\Controller\CachedRequestController;
use Symfony\Component\Routing\RouteCollection;

/**
 * Override the controller for the /graphql route.
 */
final class GraphQLRouteSubscriber extends RouteSubscriberBase {

  /**
   * The GraphQL server storage.
   */
  private readonly EntityStorageInterface $graphqlServerStorage;

  /**
   * Constructs a new object.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->graphqlServerStorage = $entityTypeManager->getStorage('graphql_server');
  }

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection): void {
    /** @var \Drupal\graphql\Entity\ServerInterface[] $servers */
    $servers = $this->graphqlServerStorage->loadMultiple();

    $allowed_ids = ['task'];
    foreach ($servers as $id => $server) {
      // @phpstan-ignore-next-line
      $schema = $server->schema;
      if ($schema !== 'core_composable' || !in_array($id, $allowed_ids, TRUE)) {
        return;
      }

      $route = $collection->get("graphql.query.{$id}");
      $route?->setDefault(
        '_controller',
        CachedRequestController::class . '::handleRequest'
      );
    }
  }

}
