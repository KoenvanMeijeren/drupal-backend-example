<?php

namespace Drupal\task\Plugin\GraphQL\Schema;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistry;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\Schema\SdlSchemaPluginBase;
use Drupal\task\Wrappers\TaskQueryConnection;

/**
 * Provides the task schema.
 *
 * @Schema(
 *   id = "task",
 *   name = "Task schema"
 * )
 */
final class TaskSchema extends SdlSchemaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getResolverRegistry(): ResolverRegistryInterface {
    $builder = new ResolverBuilder();
    $registry = new ResolverRegistry();

    $this->addQueryFields($registry, $builder);
    $this->addTaskFields($registry, $builder);

    // Re-usable connection type fields.
    $this->addConnectionFields('TaskConnection', $registry, $builder);

    return $registry;
  }

  /**
   * Adds resolvers for the task type.
   */
  private function addTaskFields(ResolverRegistry $registry, ResolverBuilder $builder): void {
    $registry->addFieldResolver('Task', 'id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('Task', 'uuid',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('Task', 'title',
      $builder->compose(
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent()),
        $builder->produce('uppercase')
          ->map('string', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('Task', 'author',
      $builder->compose(
        $builder->produce('entity_owner')
          ->map('entity', $builder->fromParent()),
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('Task', 'published',
      $builder->compose(
        $builder->produce('entity_published')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('Task', 'description',
      $builder->compose(
        $builder->produce('task_description')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('Task', 'created',
      $builder->compose(
        $builder->produce('entity_created')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('Task', 'changed',
      $builder->compose(
        $builder->produce('entity_changed')
          ->map('entity', $builder->fromParent())
      )
    );
  }

  /**
   * Adds resolvers for the query type.
   */
  private function addQueryFields(ResolverRegistry $registry, ResolverBuilder $builder): void {
    $registry->addFieldResolver('Query', 'task',
      $builder->produce('entity_load')
        ->map('type', $builder->fromValue('task'))
        ->map('id', $builder->fromArgument('id'))
    );

    $registry->addFieldResolver('Query', 'tasks',
      $builder->produce('query_tasks')
        ->map('offset', $builder->fromArgument('offset'))
        ->map('limit', $builder->fromArgument('limit'))
    );
  }

  /**
   * Adds resolvers for the connection type.
   */
  private function addConnectionFields(string $type, ResolverRegistry $registry, ResolverBuilder $builder): void {
    $registry->addFieldResolver($type, 'total',
      $builder->callback(static function (TaskQueryConnection $connection) {
        return $connection->total();
      })
    );

    $registry->addFieldResolver($type, 'items',
      $builder->callback(static function (TaskQueryConnection $connection) {
        return $connection->items();
      })
    );
  }

}
