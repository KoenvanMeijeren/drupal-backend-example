<?php

namespace Drupal\task\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\task\Wrappers\TaskQueryConnection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the 'query_tasks' data producer.
 *
 * @DataProducer(
 *   id = "query_tasks",
 *   name = @Translation("Load tasks"),
 *   description = @Translation("Loads a list of tasks."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Task connection")
 *   ),
 *   consumes = {
 *     "offset" = @ContextDefinition("integer",
 *       label = @Translation("Offset"),
 *       required = FALSE
 *     ),
 *     "limit" = @ContextDefinition("integer",
 *       label = @Translation("Limit"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
final class QueryTasks extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = new self($configuration, $plugin_id, $plugin_definition);

    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * Resolve the list of tasks.
   */
  public function resolve(int $offset, int $limit, RefinableCacheableDependencyInterface $metadata): TaskQueryConnection {
    $storage = $this->entityTypeManager->getStorage('task');
    $entityType = $storage->getEntityType();
    $query = $storage->getQuery()
      ->currentRevision()
      ->accessCheck()
      // The access check does not filter out unpublished nodes automatically,
      // so we need to do this explicitly here. We don't want to run access
      // checks on loaded nodes later, as that would then make the query count
      // numbers wrong. Therefore, all fields relevant for access need to be
      // included here.
      ->condition('status', TRUE);

    if ($limit > 0) {
      $query->range($offset, $limit);
    }

    $metadata->addCacheTags($entityType->getListCacheTags());
    $metadata->addCacheContexts($entityType->getListCacheContexts());

    return new TaskQueryConnection($query);
  }

}
