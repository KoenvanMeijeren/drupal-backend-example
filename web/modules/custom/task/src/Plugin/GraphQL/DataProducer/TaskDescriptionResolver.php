<?php

namespace Drupal\task\Plugin\GraphQL\DataProducer;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\task\Entity\TaskInterface;
use Drupal\task_nested\Entity\TaskNestedInterface;

/**
 * Returns the description text of an entity.
 *
 * @DataProducer(
 *   id = "task_description",
 *   name = @Translation("Task description"),
 *   description = @Translation("Returns the task description."),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("Description")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Task")
 *     )
 *   }
 * )
 */
class TaskDescriptionResolver extends DataProducerPluginBase {

  /**
   * Resolver.
   */
  public function resolve(TaskInterface|TaskNestedInterface $entity): ?string {
    return $entity->getDescription();
  }

}
