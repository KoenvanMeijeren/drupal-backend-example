<?php

namespace Drupal\task\Wrappers;

use Drupal\Core\Entity\Query\QueryInterface;
use GraphQL\Deferred;

/**
 * Helper class that wraps entity queries.
 */
final readonly class TaskQueryConnection {

  /**
   * QueryConnection constructor.
   */
  public function __construct(
    private QueryInterface $query,
  ) {}

  /**
   * Returns the total number of items in the query.
   */
  public function total(): int {
    $query = clone $this->query;
    $query->count();
    /** @var int */
    return $query->execute();
  }

  /**
   * Returns the items in the query.
   */
  public function items(): array|Deferred {
    $result = $this->query->execute();
    if ($result === []) {
      return [];
    }

    // @phpstan-ignore-next-line
    $buffer = \Drupal::service('graphql.buffer.entity');
    $callback = $buffer->add($this->query->getEntityTypeId(), array_values((array) $result));
    return new Deferred(static function () use ($callback) {
      return $callback();
    });
  }

}
