<?php

namespace Drupal\task\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\general\Api\Validation\EntityValidationErrorItemsDto;
use Drupal\general\Api\Validation\ValidationErrorResponse;
use Drupal\task\Api\Collection\TaskCollection;
use Drupal\task\Api\Collection\TaskCollectionGenerated;
use Drupal\task\Api\Input\TaskApiInputDto;
use Drupal\task\Entity\Task;
use Drupal\task\Storage\TaskStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a class for TaskApiController.
 */
final class TaskApiController extends ControllerBase {

  /**
   * The task storage.
   */
  private TaskStorageInterface $taskStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);

    $instance->taskStorage = $instance->entityTypeManager()->getStorage('task');

    return $instance;
  }

  /**
   * Returns the collection of tasks.
   */
  public function collectionV1(Request $request): JsonResponse {
    $page = $request->query->getInt('page', -1);
    $limit = $request->query->getInt('limit', 50);
    $entities = $page === -1
      ? $this->taskStorage->loadByProperties()
      : $this->taskStorage->loadPaginated($page, $limit);

    return new JsonResponse(TaskCollection::fromEntities($entities), Response::HTTP_OK);
  }

  /**
   * Returns the collection of tasks.
   */
  public function collectionWithGeneratorV1(Request $request): JsonResponse {
    $page = $request->query->getInt('page', -1);
    $limit = $request->query->getInt('limit', 50);
    $entities = $page === -1
      ? $this->taskStorage->loadByProperties()
      : $this->taskStorage->loadPaginated($page, $limit);

    return new JsonResponse(TaskCollectionGenerated::fromEntities($entities), Response::HTTP_OK);
  }

  /**
   * Create a task.
   */
  public function createV1(Request $request): JsonResponse {
    $input_dto = TaskApiInputDto::fromRequest($request);
    $task = Task::create($input_dto->toArray());

    $validation_errors = EntityValidationErrorItemsDto::createFrom($task->validate());
    if ($validation_errors->hasErrors) {
      return new ValidationErrorResponse($validation_errors);
    }

    $task->save();

    return new JsonResponse('Task created successfully', Response::HTTP_CREATED);
  }

  /**
   * Updates a task.
   */
  public function updateV1(string $task_uuid, Request $request): JsonResponse {
    $task = $this->taskStorage->loadByUuid($task_uuid);
    if ($task === NULL) {
      return new JsonResponse('Task not found', Response::HTTP_NOT_FOUND);
    }

    $input_dto = TaskApiInputDto::fromRequest($request);
    $task->setValues($input_dto->toArray());

    $validation_errors = EntityValidationErrorItemsDto::createFrom($task->validate());
    if ($validation_errors->hasErrors) {
      return new ValidationErrorResponse($validation_errors);
    }

    $task->save();

    return new JsonResponse('Task updated successfully', Response::HTTP_OK);
  }

  /**
   * Deletes a task.
   */
  public function deleteV1(string $task_uuid): JsonResponse {
    $task = $this->taskStorage->loadByUuid($task_uuid);
    if ($task === NULL) {
      return new JsonResponse('Task not found', Response::HTTP_NOT_FOUND);
    }

    $task->delete();

    return new JsonResponse('', Response::HTTP_NO_CONTENT);
  }

}
