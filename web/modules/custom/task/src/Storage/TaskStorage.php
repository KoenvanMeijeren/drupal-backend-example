<?php

namespace Drupal\task\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\task\Entity\TaskInterface;

/**
 * Defines the storage handler class for Task entities.
 *
 * This extends the base storage class, adding required special handling for
 * Task entities.
 *
 * @ingroup task
 */
final class TaskStorage extends SqlContentEntityStorage implements TaskStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByUuid(string $uuid): ?TaskInterface {
    $entities = $this->loadByProperties(['uuid' => $uuid]);
    if ($entities === []) {
      return NULL;
    }

    // @phpstan-ignore-next-line
    return reset($entities) ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadPaginated(int $page, int $limit): array {
    $query = $this->getQuery();
    $query->pager($limit, $page);
    $query->sort('created', 'DESC');
    $query->accessCheck();

    return $this->loadMultiple($query->execute());
  }

}
