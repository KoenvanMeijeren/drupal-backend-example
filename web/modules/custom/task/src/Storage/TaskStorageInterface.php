<?php

namespace Drupal\task\Storage;

use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\task\Entity\TaskInterface;

/**
 * Defines the storage handler class for Task entities.
 *
 * This extends the base storage class, adding required special handling for
 * Task entities.
 *
 * @ingroup task
 */
interface TaskStorageInterface extends SqlEntityStorageInterface {

  /**
   * Loads a task by its UUID.
   */
  public function loadByUuid(string $uuid): ?TaskInterface;

  /**
   * Loads tasks with pagination.
   *
   * @return \Drupal\task\Entity\TaskInterface[]
   *   The tasks.
   */
  public function loadPaginated(int $page, int $limit): array;

}
