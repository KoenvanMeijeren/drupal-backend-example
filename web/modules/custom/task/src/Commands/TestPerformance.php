<?php

namespace Drupal\task\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\task\Storage\TaskStorageInterface;
use Drush\Attributes as CLI;
use Faker\Factory as FakerFactory;
use GuzzleHttp\Client;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * {@inheritDoc}
 */
final class TestPerformance extends TestPerformanceCommandBase {

  /**
   * The task storage.
   */
  private readonly TaskStorageInterface $taskStorage;

  /**
   * Constructs a new object.
   */
  public function __construct(Client $httpClient, EntityTypeManagerInterface $entityTypeManager,) {
    parent::__construct($httpClient, $entityTypeManager);

    $this->taskStorage = $this->entityTypeManager->getStorage('task');
  }

  /**
   * Test the API's with various scenarios and show the results in the console.
   */
  #[CLI\Command(name: 'test:task:performance')]
  #[CLI\Usage(name: 'drush test:task:performance', description: 'Tests the performance of task endpoints.')]
  public function executePerformanceTest(): void {
    $this->execute([10, 100, 1000], 5, 10, 'task_execution_times.json');
  }

  /**
   * Test the API's with various scenarios and show the results in the console.
   */
  #[CLI\Command(name: 'test:task:performance-results')]
  #[CLI\Usage(name: 'drush test:task:performance-results', description: 'Shows the results of the performance tests.')]
  public function executeResultsDisplayBasedOnExistingFile(): void {
    $executionTimesFile = 'task_execution_times.json';
    $executionTimes = (array) json_decode((string) file_get_contents($executionTimesFile), TRUE, 512, JSON_THROW_ON_ERROR);
    $this->executionTimes = $executionTimes;
    $output = $this->output();
    $this->displaySummaryTable($output);
    $this->displaySummarySummaryTable($output);
  }

  /**
   * {@inheritDoc}
   */
  protected function testApi(OutputInterface $output, int $maxRunsPerTest, int $sleepTime, array $entityCounts): void {
    $table = new Table($output);
    $table->setHeaders(['Entities', 'Scenario', 'API type', 'Run', 'Execution Time (ms)']);

    foreach ($entityCounts as $numEntities) {
      $this->seedDatabase($numEntities);
      $bearerToken = $this->getAccessToken();

      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task/task', $bearerToken, $maxRunsPerTest, 0, 'Cleared Cache', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task/task', $bearerToken, $maxRunsPerTest, $sleepTime, 'Cleared Cache waited', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task/task', $bearerToken, $maxRunsPerTest, 0, 'Normal', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task/task', $bearerToken, $maxRunsPerTest, $sleepTime, 'Normal waited', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task/task?page[limit]=50&page[offset]=0', $bearerToken, $maxRunsPerTest, 0, 'Paginated', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task/task?page[limit]=50&page[offset]=0', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated waited', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task/task?page[limit]=50&page[offset]=0', $bearerToken, $maxRunsPerTest, 0, 'Paginated Cleared Cache', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task/task?page[limit]=50&page[offset]=0', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated Cleared Cache waited', $numEntities);

      $this->testScenario($table, $output, 'custom', 'http://nginx/api/v1/tasks', $bearerToken, $maxRunsPerTest, 0, 'Cleared Cache', $numEntities);
      $this->testScenario($table, $output, 'custom', 'http://nginx/api/v1/tasks', $bearerToken, $maxRunsPerTest, $sleepTime, 'Cleared Cache waited', $numEntities);
      $this->testScenario($table, $output, 'custom', 'http://nginx/api/v1/tasks', $bearerToken, $maxRunsPerTest, 0, 'Normal', $numEntities);
      $this->testScenario($table, $output, 'custom', 'http://nginx/api/v1/tasks', $bearerToken, $maxRunsPerTest, $sleepTime, 'Normal waited', $numEntities);
      $this->testScenario($table, $output, 'custom', 'http://nginx/api/v1/tasks?limit=50&page=1', $bearerToken, $maxRunsPerTest, 0, 'Paginated', $numEntities);
      $this->testScenario($table, $output, 'custom', 'http://nginx/api/v1/tasks?limit=50&page=1', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated waited', $numEntities);
      $this->testScenario($table, $output, 'custom', 'http://nginx/api/v1/tasks?limit=50&page=1', $bearerToken, $maxRunsPerTest, 0, 'Paginated Cleared Cache', $numEntities);
      $this->testScenario($table, $output, 'custom', 'http://nginx/api/v1/tasks?limit=50&page=1', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated Cleared Cache waited', $numEntities);

      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task', $bearerToken, $maxRunsPerTest, 0, 'Cleared Cache', $numEntities, '{"query":"query {\n  tasks(offset: 0, limit: -1) {\n    total\n    items {\n      id\n      title\n      author\n    }\n  }\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task', $bearerToken, $maxRunsPerTest, $sleepTime, 'Cleared Cache waited', $numEntities, '{"query":"query {\n  tasks(offset: 0, limit: -1) {\n    total\n    items {\n      id\n      title\n      author\n    }\n  }\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task', $bearerToken, $maxRunsPerTest, 0, 'Normal', $numEntities, '{"query":"query {\n  tasks(offset: 0, limit: -1) {\n    total\n    items {\n      id\n      title\n      author\n    }\n  }\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task', $bearerToken, $maxRunsPerTest, $sleepTime, 'Normal waited', $numEntities, '{"query":"query {\n  tasks(offset: 0, limit: -1) {\n    total\n    items {\n      id\n      title\n      author\n    }\n  }\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task', $bearerToken, $maxRunsPerTest, 0, 'Paginated', $numEntities, '{"query":"query {\n  tasks(offset: 0, limit: 50) {\n    total\n    items {\n      id\n      title\n      author\n    }\n  }\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated waited', $numEntities, '{"query":"query {\n  tasks(offset: 0, limit: 50) {\n    total\n    items {\n      id\n      title\n      author\n    }\n  }\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task', $bearerToken, $maxRunsPerTest, 0, 'Paginated Cleared Cache', $numEntities, '{"query":"query {\n  tasks(offset: 0, limit: 50) {\n    total\n    items {\n      id\n      title\n      author\n    }\n  }\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated Cleared Cache waited', $numEntities, '{"query":"query {\n  tasks(offset: 0, limit: 50) {\n    total\n    items {\n      id\n      title\n      author\n    }\n  }\n}"}');
    }

    $output->writeln('');
    $output->writeln('<fg=blue>Results of the performance test of task endpoints...</>');
    $table->render();

    $output->writeln('');
    $output->writeln('<fg=blue>Summary of the performance of task endpoints...</>');
    $this->displaySummaryTable($output);

    $output->writeln('');
    $output->writeln('<fg=blue>Summary of the summary of the performance of task endpoints...</>');
    $this->displaySummarySummaryTable($output);
  }

  /**
   * {@inheritDoc}
   */
  protected function seedDatabase(int $amount, int $retry = 0): void {
    $output = $this->output();
    $faker = FakerFactory::create();

    $output->writeln('<comment>Seeding the database with task entities...</comment>');

    $output->writeln('<error>Deleting all existing task entities...</error>');

    try {
      // @phpstan-ignore-next-line
      \Drupal::database()->truncate('task')->execute(); //phpcs:ignore

      $output->writeln("<comment>Creating {$amount} new task entities...</comment>");
      for ($index = 0; $index < $amount; $index++) {
        $this->taskStorage->create([
          'label' => $faker->words(3, TRUE),
          'description' => $faker->paragraph(10),
          'status' => TRUE,
        ])->save();
      }
    }
    catch (\Throwable $exception) {
      $output->writeln("<error>Failed to seed the database with task entities: {$exception->getMessage()}</error>");

      if ($retry < 5) {
        $output->writeln('<error>Retrying...</error>');
        sleep(10);
        $this->seedDatabase($amount, $retry + 1);
        return;
      }

      throw $exception;
    }

    $output->writeln('<info>Database seeded with task entities.</info>');
  }

}
