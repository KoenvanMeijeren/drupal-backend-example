<?php

namespace Drupal\task\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\task\Storage\TaskStorageInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Faker\Factory as FakerFactory;

/**
 * Provides a class for seeding the database with task entities.
 *
 * @package Drupal\general\Command
 */
final class SeedDatabase extends DrushCommands {

  /**
   * The task storage.
   */
  private readonly TaskStorageInterface $taskStorage;

  /**
   * Constructs a new object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly Connection $connection,
  ) {
    parent::__construct();

    $this->taskStorage = $this->entityTypeManager->getStorage('task');
  }

  /**
   * Updates the config.
   */
  #[CLI\Command(name: 'task:seed:database')]
  #[CLI\Usage(name: 'drush task:seed:database', description: 'Seeds the database with task entities.')]
  #[CLI\Argument(name: 'amount', description: 'The amount of task entities to seed the database with.')]
  public function execute(int $amount): void {
    $output = $this->output();
    $faker = FakerFactory::create();

    $output->writeln('<info>Seeding the database with task entities...</info>');

    $output->writeln('<info>Deleting all existing task entities...</info>');
    $this->connection->truncate('task')->execute();

    $output->writeln("<info>Creating {$amount} new task entities...</info>");
    for ($index = 0; $index < $amount; $index++) {
      $this->taskStorage->create([
        'label' => $faker->words(3, TRUE),
        'description' => $faker->paragraph(10),
        'status' => TRUE,
      ])->save();

      if ($index % 25 === 0) {
        $output->writeln("<info>Created {$index} task entities...</info>");
      }
    }

    $output->writeln('<info>Database seeded with task entities.</info>');
  }

}
