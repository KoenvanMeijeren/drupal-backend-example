<?php

namespace Drupal\task\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\jsonapi\Query\OffsetPage;
use Drush\Commands\DrushCommands;
use GuzzleHttp\Client;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Provides a class for the update config command.
 *
 * @package Drupal\task\Command
 */
abstract class TestPerformanceCommandBase extends DrushCommands {

  /**
   * Add a property to store execution times.
   */
  protected array $executionTimes = [];

  /**
   * Constructs a new object.
   */
  public function __construct(
    protected readonly Client $httpClient,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct();
  }

  /**
   * Test the API's with various scenarios and show the results in the console.
   */
  protected function execute(array $numEntities, int $maxRuns, int $sleepTime, string $dumpFilename): void {
    $output = $this->output();

    $output->writeln('<fg=blue>Testing the performance of task endpoints...</>');

    $this->testApi($output, $maxRuns, $sleepTime, $numEntities);

    $this->dumpExecutionTimes($output, $dumpFilename);

    $output->writeln('<fg=blue>Testing the performance of task endpoints... Done</>');
  }

  /**
   * Test the API's with various scenarios and write the results to the console.
   */
  abstract protected function testApi(OutputInterface $output, int $maxRunsPerTest, int $sleepTime, array $entityCounts): void;

  /**
   * Seed the database with entities.
   */
  abstract protected function seedDatabase(int $amount, int $retry = 0): void;

  /**
   * Test a specific scenario and write the average time to the console.
   *
   * @SuppressWarnings(PHPMD.ExcessiveParameterList)
   * @SuppressWarnings(PHPMD.CyclomaticComplexity)
   */
  protected function testScenario(Table $table, OutputInterface $output, string $apiType, string $endpoint, string $bearerToken, int $maxRunsPerTest, int $sleepTime, string $scenario, int $numEntities, string $grahpql_query = ''): void {
    // @phpstan-ignore-next-line
    if ($apiType === 'jsonApi' && OffsetPage::SIZE_MAX <= 50) {
      $this->executionTimes[$numEntities][$scenario][$apiType][] = 0;
      $output->writeln('<error>JSON:API pagination is not overridable, therefore the tests are not trustable and cannot be run. Skipping JSON:API for now.</error>');
      return;
    }

    $color = match ($scenario) {
      'Paginated' => 'yellow',
      'Paginated waited' => 'red',
      'Paginated Cleared Cache' => 'bright-yellow',
      'Paginated Cleared Cache waited' => 'bright-magenta',
      'Normal' => 'green',
      'Normal waited' => 'bright-green',
      'Cleared Cache' => 'cyan',
      'Cleared Cache waited' => 'magenta',
      default => 'white',
    };

    $apiTypeColor = match ($apiType) {
      'jsonApi' => 'blue',
      'custom', 'custom-entity-manager' => 'red',
      'custom-manually' => 'bright-red',
      'graphql' => 'green',
      default => 'white',
    };

    for ($runIndex = 1; $runIndex <= $maxRunsPerTest; $runIndex++) {
      $output->writeln("<fg=blue>Running {$apiType} scenario: {$scenario} with {$numEntities} entities (run {$runIndex} of {$maxRunsPerTest})</>");
      sleep($sleepTime);

      switch ($scenario) {
        case 'Paginated':
        case 'Paginated waited':
        case 'Normal':
        case 'Normal waited':
          $this->runScenario($apiType, $endpoint, $bearerToken, $table, $scenario, $apiTypeColor, $numEntities, $grahpql_query);
          break;

        case 'Paginated Cleared Cache':
        case 'Paginated Cleared Cache waited':
        case 'Cleared Cache':
        case 'Cleared Cache waited':
          $this->clearCache($output);
          $this->runScenario($apiType, $endpoint, $bearerToken, $table, $scenario, $apiTypeColor, $numEntities, $grahpql_query);
          break;

        default:
          throw new \InvalidArgumentException("Invalid scenario: {$scenario}");
      }
    }

    $executionTimes = $this->executionTimes[$numEntities][$scenario][$apiType] ?? [];
    $averageExecutionTime = $executionTimes !== [] ? array_sum($executionTimes) / count($executionTimes) : 0;
    $averageExecutionTimeString = str_pad(number_format($averageExecutionTime, 2, ',', '.'), 8, " ", STR_PAD_LEFT);

    $table->addRow([
      "<fg={$color}>{$numEntities}</>",
      "<fg={$color}>{$scenario}</>",
      "<fg={$apiTypeColor}>{$apiType}</>",
      "<fg={$color}>Average</>",
      "<fg={$color}>{$averageExecutionTimeString}</>",
    ]);
  }

  /**
   * Run the normal scenario.
   */
  protected function runScenario(string $apiType, string $endpoint, string $bearerToken, Table $table, string $scenario, string $apiTypeColor, int $numEntities, string $grahpql_query = ''): void {
    $startTime = hrtime(TRUE);
    $is_successful = $this->executeRequest($apiType, $endpoint, $bearerToken, $table, $scenario, $apiTypeColor, $numEntities, $grahpql_query);
    $endTime = hrtime(TRUE);

    $executionTime = 30_000;
    if ($is_successful) {
      $executionTime = ($endTime - $startTime) / 1_000_000;
      $this->accumulateExecutionTime($apiType, $scenario, $executionTime, $numEntities);
    }

    $executionTimeString = str_pad(number_format($executionTime, 2, ',', '.'), 8, " ", STR_PAD_LEFT);

    $table->addRow([
      "<fg=default>{$numEntities}</>",
      "<fg=default>{$scenario}</>",
      "<fg={$apiTypeColor}>{$apiType}</>",
      "<fg=default>Execution time</>",
      "<fg=default>{$executionTimeString}</>",
    ]);
  }

  /**
   * Execute the request.
   */
  protected function executeRequest(string $apiType, string $endpoint, string $bearerToken, Table $table, string $scenario, string $apiTypeColor, int $numEntities, string $grahpql_query = ''): bool {
    try {
      if ($apiType === 'graphql') {
        $response = $this->httpClient->request('POST', $endpoint, [
          'headers' => [
            'Authorization' => 'Bearer ' . $bearerToken,
            'Content-Type' => 'application/json',
          ],
          'body' => $grahpql_query,
        ])->getBody()->getContents();

        if (str_contains($response, 'errors') && str_contains($response, 'GraphQL')) {
          throw new \RuntimeException('GraphQL error: ' . $response);
        }
        return TRUE;
      }

      $this->httpClient->request('GET', $endpoint, [
        'headers' => [
          'Authorization' => "Bearer {$bearerToken}",
        ],
      ]);
    }
    catch (\Throwable $exception) {
      $isTimeoutError = str_contains($exception->getMessage(), 'cURL error 28: Operation timed out');
      if (!$isTimeoutError && !str_contains($exception->getMessage(), 'Pagination is not supported')) {
        throw $exception;
      }

      $this->executionTimes[$numEntities][$scenario][$apiType][] = 30_000;

      $table->addRow([
        "<fg=default>{$numEntities}</>",
        "<fg=default>{$scenario}</>",
        "<fg={$apiTypeColor}>{$apiType}</>",
        "<fg=default>Execution time</>",
        $isTimeoutError ? "<fg=default>Timeout error</>" : "<fg=default>Error</>",
      ]);

      return FALSE;
    }

    return TRUE;
  }

  /**
   * Display the summary table.
   */
  protected function displaySummaryTable(OutputInterface $output): void {
    $summaryTable = new Table($output);
    $summaryTable->setHeaders(['Entities', 'Scenario', 'API type', 'Average Execution Time (ms)']);

    foreach ($this->executionTimes as $numEntities => $scenarios) {
      foreach ($scenarios as $scenario => $apiTypes) {
        foreach ($apiTypes as $apiType => $executionTimes) {
          $color = match ($scenario) {
            'Paginated' => 'yellow',
            'Paginated waited' => 'red',
            'Paginated Cleared Cache' => 'bright-yellow',
            'Paginated Cleared Cache waited' => 'bright-magenta',
            'Normal' => 'green',
            'Normal waited' => 'bright-green',
            'Cleared Cache' => 'cyan',
            'Cleared Cache waited' => 'magenta',
            default => 'white',
          };

          $apiTypeColor = match ($apiType) {
            'jsonApi' => 'blue',
            'custom', 'custom-entity-manager' => 'red',
            'custom-manually' => 'bright-red',
            'graphql' => 'green',
            default => 'white',
          };

          $averageExecutionTime = array_sum($executionTimes) / count($executionTimes);
          $averageExecutionTimeString = str_pad(number_format($averageExecutionTime, 2, ',', '.'), 10, " ", STR_PAD_LEFT);
          $numEntitiesString = str_pad(number_format($numEntities, 0, ',', '.'), 8, " ", STR_PAD_LEFT);

          $summaryTable->addRow([
            "<fg={$color}>{$numEntitiesString}</>",
            "<fg={$color}>{$scenario}</>",
            "<fg={$apiTypeColor}>{$apiType}</>",
            "<fg={$color}>{$averageExecutionTimeString}</>",
          ]);
        }
      }
    }

    $summaryTable->render();
  }

  /**
   * Display the summary of the summary table.
   */
  protected function displaySummarySummaryTable(OutputInterface $output): void {
    $summaryData = [];

    // Initialize summary data structure.
    foreach ($this->executionTimes as $numEntities => $scenarios) {
      foreach ($scenarios as $scenario => $apiTypes) {
        $summaryData[$numEntities][$scenario] = ['fastest' => NULL, 'time' => PHP_INT_MAX];
      }
    }

    // Populate summary data.
    foreach ($this->executionTimes as $numEntities => $scenarios) {
      foreach ($scenarios as $scenario => $apiTypes) {
        foreach ($apiTypes as $apiType => $executionTimes) {
          $averageExecutionTime = array_sum($executionTimes) / count($executionTimes);
          if ($averageExecutionTime < $summaryData[$numEntities][$scenario]['time']) {
            $summaryData[$numEntities][$scenario]['fastest'] = $apiType;
            $summaryData[$numEntities][$scenario]['time'] = $averageExecutionTime;
          }
        }
      }
    }

    $summaryTable = new Table($output);
    $summaryTable->setHeaders(['Entities', 'Scenario', 'Fastest API Type', 'Average Execution Time (ms)']);

    foreach ($summaryData as $numEntities => $scenarios) {
      foreach ($scenarios as $scenario => $data) {
        $color = match ($scenario) {
          'Paginated' => 'yellow',
          'Paginated waited' => 'red',
          'Paginated Cleared Cache' => 'bright-yellow',
          'Paginated Cleared Cache waited' => 'bright-magenta',
          'Normal' => 'green',
          'Normal waited' => 'bright-green',
          'Cleared Cache' => 'cyan',
          'Cleared Cache waited' => 'magenta',
          default => 'white',
        };

        $apiType = $data['fastest'] ?? 'N/A';
        $time = $data['time'] !== PHP_INT_MAX ? number_format($data['time'], 2, ',', '.') : 'N/A';

        $numEntitiesString = str_pad(number_format($numEntities, 0, ',', '.'), 8, " ", STR_PAD_LEFT);
        $timeString = str_pad($time, 10, " ", STR_PAD_LEFT);

        $apiTypeColor = match ($apiType) {
          'jsonApi' => 'blue',
          'custom', 'custom-entity-manager' => 'red',
          'custom-manually' => 'bright-red',
          'graphql' => 'green',
          default => 'white',
        };

        $summaryTable->addRow([
          "<fg={$color}>{$numEntitiesString}</>",
          "<fg={$color}>{$scenario}</>",
          "<fg={$apiTypeColor}>{$apiType}</>",
          "<fg={$color}>{$timeString}</>",
        ]);
      }
    }

    $summaryTable->render();
  }

  /**
   * Dump the execution times to a file.
   */
  protected function dumpExecutionTimes(OutputInterface $output, string $filename): void {
    $output->writeln('<comment>Dumping the execution times to a file...</comment>');
    file_put_contents($filename, json_encode($this->executionTimes, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));
    $output->writeln("<info>Execution times dumped to {$filename}</info>");
  }

  /**
   * Accumulate the execution time.
   */
  protected function accumulateExecutionTime(string $apiType, string $scenario, float $executionTime, int $numEntities): void {
    $this->executionTimes[$numEntities][$scenario][$apiType][] = $executionTime;
  }

  /**
   * Clear the cache.
   */
  protected function clearCache(OutputInterface $output, int $retry = 3): void {
    $output->writeln('<comment>Clearing the cache...</comment>');
    try {
      drupal_flush_all_caches();
    }
    catch (\Throwable $exception) {
      $output->writeln("<error>Failed to clear the cache: {$exception->getMessage()}</error>");

      if ($retry < 5) {
        $output->writeln('<error>Retrying...</error>');
        sleep(10);
        $this->clearCache($output, $retry + 1);
      }

      return;
    }
    $output->writeln('<info>Clearing the cache... Done</info>');
  }

  /**
   * Get the access token.
   */
  protected function getAccessToken(): string {
    $response_string = $this->httpClient->request('POST', 'http://nginx/oauth/token', [
      'form_params' => [
        'grant_type' => 'password',
        'client_id' => 'default_consumer',
        'client_secret' => 'nimda',
        'username' => 'admin',
        'password' => 'nimda',
        'scope' => '',
      ],
    ])
      ->getBody()
      ->getContents();
    $response = json_decode($response_string, TRUE, 512, JSON_THROW_ON_ERROR);

    // @phpstan-ignore-next-line
    return $response['access_token'] ?? '';
  }

}
