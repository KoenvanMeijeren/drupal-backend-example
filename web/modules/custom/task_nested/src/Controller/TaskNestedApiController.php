<?php

namespace Drupal\task_nested\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\general\Api\Validation\EntityValidationErrorItemsDto;
use Drupal\general\Api\Validation\ValidationErrorResponse;
use Drupal\task_nested\Api\Collection\TaskNestedCollection;
use Drupal\task_nested\Api\Collection\TaskNestedCollectionGenerated;
use Drupal\task_nested\Api\Collection\TaskNestedManualCollection;
use Drupal\task_nested\Api\Input\TaskNestedApiInputDto;
use Drupal\task_nested\Entity\TaskNested;
use Drupal\task_nested\Storage\TaskNestedStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a class for TaskApiController.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
final class TaskNestedApiController extends ControllerBase {

  /**
   * The task storage.
   */
  private TaskNestedStorageInterface $taskStorage;

  /**
   * The database.
   */
  private Connection $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);

    $instance->database = $container->get('database');
    $instance->taskStorage = $instance->entityTypeManager()->getStorage('task_nested');

    return $instance;
  }

  /**
   * Returns the collection of tasks.
   */
  public function collectionV1(Request $request): JsonResponse {
    $page = $request->query->getInt('page', -1);
    $limit = $request->query->getInt('limit', 50);
    $entities = $page === -1
      ? $this->taskStorage->loadByProperties()
      : $this->taskStorage->loadPaginated($page, $limit);

    return new JsonResponse(TaskNestedCollection::fromEntities($entities), Response::HTTP_OK);
  }

  /**
   * Returns the collection of tasks.
   */
  public function manuallyCollection(Request $request): JsonResponse {
    $page = $request->query->getInt('page', -1);
    $limit = $request->query->getInt('limit', -1);
    if ($page !== -1 || $limit !== -1) {
      throw new \Exception('Pagination is not supported');
    }

    // @phpstan-ignore-next-line
    $tasks = $this->database->query('
SELECT tnl.entity_id as "tnl_entity_id", tn.id as "tn_entity_id", t.*
FROM task_nested__lines AS tnl
INNER JOIN task_nested AS tn ON tnl.entity_id = tn.id
INNER JOIN task AS t ON tnl.lines_target_id = t.id
')?->fetchAllAssoc("id", \PDO::FETCH_NAMED);
    // @phpstan-ignore-next-line
    $nested_task_lines = $this->database->query('
SELECT tnl.entity_id as "tnl_entity_id", tn.id as "tn_entity_id", tn.*, t.id as "t_id"
FROM task_nested__lines AS tnl
INNER JOIN task_nested AS tn ON tnl.entity_id = tn.id
INNER JOIN task AS t ON tnl.lines_target_id = t.id
')?->fetchAll(\PDO::FETCH_NAMED);

    // @phpstan-ignore-next-line
    return new JsonResponse(TaskNestedManualCollection::fromEntities($nested_task_lines, $tasks), Response::HTTP_OK);
  }

  /**
   * Returns the collection of tasks.
   */
  public function collectionWithGeneratorV1(Request $request): JsonResponse {
    $page = $request->query->getInt('page', -1);
    $limit = $request->query->getInt('limit', 50);
    $entities = $page === -1
      ? $this->taskStorage->loadByProperties()
      : $this->taskStorage->loadPaginated($page, $limit);

    return new JsonResponse(TaskNestedCollectionGenerated::fromEntities($entities), Response::HTTP_OK);
  }

  /**
   * Create a task.
   */
  public function createV1(Request $request): JsonResponse {
    $input_dto = TaskNestedApiInputDto::fromRequest($request);
    $task = TaskNested::create($input_dto->toArray());

    $validation_errors = EntityValidationErrorItemsDto::createFrom($task->validate());
    if ($validation_errors->hasErrors) {
      return new ValidationErrorResponse($validation_errors);
    }

    $task->save();

    return new JsonResponse('Task nested created successfully', Response::HTTP_CREATED);
  }

  /**
   * Updates a task.
   */
  public function updateV1(string $task_uuid, Request $request): JsonResponse {
    $task = $this->taskStorage->loadByUuid($task_uuid);
    if ($task === NULL) {
      return new JsonResponse('Task nested not found', Response::HTTP_NOT_FOUND);
    }

    $input_dto = TaskNestedApiInputDto::fromRequest($request);
    $task->setValues($input_dto->toArray());

    $validation_errors = EntityValidationErrorItemsDto::createFrom($task->validate());
    if ($validation_errors->hasErrors) {
      return new ValidationErrorResponse($validation_errors);
    }

    $task->save();

    return new JsonResponse('Task nested updated successfully', Response::HTTP_OK);
  }

  /**
   * Deletes a task.
   */
  public function deleteV1(string $task_uuid): JsonResponse {
    $task = $this->taskStorage->loadByUuid($task_uuid);
    if ($task === NULL) {
      return new JsonResponse('Task nested not found', Response::HTTP_NOT_FOUND);
    }

    $task->delete();

    return new JsonResponse('', Response::HTTP_NO_CONTENT);
  }

}
