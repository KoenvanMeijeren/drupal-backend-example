<?php

namespace Drupal\task_nested\Api\Collection;

use Drupal\task\Api\Collection\TaskCollection;
use Drupal\task_nested\Entity\TaskNestedInterface;

/**
 * Provides a class for TaskCollection.
 */
final readonly class TaskNestedItemDto implements \JsonSerializable {

  /**
   * Constructs a new TaskItemDto.
   */
  public function __construct(
    public int|string|NULL $id,
    public ?string $uuid,
    public string $label,
    public ?string $description,
    public TaskCollection $lines,
    public bool $status,
    public int $created,
    public int $changed,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function fromArray(TaskNestedInterface $task): self {
    return new self(
      $task->id(),
      $task->uuid(),
      (string) $task->label(),
      $task->getDescription(),
      TaskCollection::fromEntities($task->getLines()),
      $task->isPublished(),
      $task->getCreatedTime(),
      $task->getChangedTime()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

  /**
   * Convert to array.
   */
  public function toArray(): array {
    return [
      'id' => $this->id,
      'uuid' => $this->uuid,
      'label' => $this->label,
      'description' => $this->description,
      'lines' => $this->lines->toArray(),
      'status' => $this->status,
      'created' => $this->created,
      'changed' => $this->changed,
    ];
  }

}
