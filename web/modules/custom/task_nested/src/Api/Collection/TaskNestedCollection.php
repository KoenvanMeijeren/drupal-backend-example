<?php

namespace Drupal\task_nested\Api\Collection;

/**
 * Provides a class for TaskCollection.
 */
final readonly class TaskNestedCollection implements \JsonSerializable {

  /**
   * Constructs a new TaskCollection.
   *
   * @param \Drupal\task_nested\Api\Collection\TaskNestedItemDto[] $tasks
   *   The tasks.
   */
  public function __construct(
    public array $tasks,
  ) {}

  /**
   * Create from array.
   */
  public static function fromEntities(array $entities): self {
    $tasks = [];
    foreach ($entities as $entity) {
      $tasks[] = TaskNestedItemDto::fromArray($entity);
    }

    return new self($tasks);
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

  /**
   * Convert to array.
   */
  public function toArray(): array {
    $tasks = [];
    foreach ($this->tasks as $task) {
      $tasks[] = $task->toArray();
    }

    return $tasks;
  }

}
