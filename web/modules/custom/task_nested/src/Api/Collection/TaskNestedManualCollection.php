<?php

namespace Drupal\task_nested\Api\Collection;

/**
 * Provides a class for TaskCollection.
 */
final readonly class TaskNestedManualCollection implements \JsonSerializable {

  /**
   * Constructs a new TaskCollection.
   */
  public function __construct(
    public array $result,
  ) {}

  /**
   * Create from array.
   */
  public static function fromEntities(array $task_nested_lines, array $tasks): self {
    $result = [];
    // Contains all task nested lines, inclusive the task nested values.
    foreach ($task_nested_lines as $task_nested_line) {
      $task = $tasks[$task_nested_line['t_id']];
      $tnl_id = $task_nested_line['tnl_entity_id'];

      // If the row already exists, add the line to the row.
      if (isset($result[$tnl_id])) {
        $result[$tnl_id]['lines'][] = TaskManualItemDto::fromArray($task)->toArray();
        continue;
      }

      // Create the row.
      $result[$tnl_id] = $task_nested_line + [
        'lines' => [
          TaskManualItemDto::fromArray($task)->toArray(),
        ],
      ];
    }

    return new self($result);
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

  /**
   * Convert to array.
   */
  public function toArray(): array {
    return $this->result;
  }

}
