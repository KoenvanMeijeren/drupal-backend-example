<?php

namespace Drupal\task_nested\Api\Collection;

/**
 * Provides a class for TaskCollection.
 */
final readonly class TaskManualItemDto implements \JsonSerializable {

  /**
   * Constructs a new TaskItemDto.
   */
  public function __construct(
    public int|string|NULL $id,
    public ?string $uuid,
    public string $label,
    public ?string $description,
    public bool $status,
    public int $created,
    public int $changed,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function fromArray(array $task): self {
    return new self(
      $task['id'],
      $task['uuid'],
      $task['label'] ?? '',
      $task['description'],
      $task['status'],
      $task['created'],
      $task['changed']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

  /**
   * Convert to array.
   */
  public function toArray(): array {
    return [
      'id' => $this->id,
      'uuid' => $this->uuid,
      'label' => $this->label,
      'description' => $this->description,
      'status' => $this->status,
      'created' => $this->created,
      'changed' => $this->changed,
    ];
  }

}
