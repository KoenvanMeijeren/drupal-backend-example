<?php

namespace Drupal\task_nested\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\task_nested\Wrappers\TaskNestedQueryConnection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the 'query_tasks' data producer.
 *
 * @DataProducer(
 *   id = "query_task_nested",
 *   name = @Translation("Load nested tasks"),
 *   description = @Translation("Loads a list of nested tasks."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Task nested connection")
 *   ),
 *   consumes = {
 *     "offset" = @ContextDefinition("integer",
 *       label = @Translation("Offset"),
 *       required = FALSE
 *     ),
 *     "limit" = @ContextDefinition("integer",
 *       label = @Translation("Limit"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
final class QueryTaskNested extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = new self($configuration, $plugin_id, $plugin_definition);

    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * Resolve the list of tasks.
   */
  public function resolve(int $offset, int $limit, RefinableCacheableDependencyInterface $metadata): TaskNestedQueryConnection {
    $storage = $this->entityTypeManager->getStorage('task_nested');
    $entityType = $storage->getEntityType();
    $query = $storage->getQuery()
      ->currentRevision()
      ->accessCheck()
      // The access check does not filter out unpublished nodes automatically,
      // so we need to do this explicitly here. We don't want to run access
      // checks on loaded nodes later, as that would then make the query count
      // numbers wrong. Therefore, all fields relevant for access need to be
      // included here.
      ->condition('status', TRUE);

    if ($limit > 0) {
      $query->range($offset, $limit);
    }

    $metadata->addCacheTags($entityType->getListCacheTags());
    $metadata->addCacheContexts($entityType->getListCacheContexts());

    return new TaskNestedQueryConnection($query);
  }

}
