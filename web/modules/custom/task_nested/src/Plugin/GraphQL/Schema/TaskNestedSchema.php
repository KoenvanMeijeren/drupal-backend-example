<?php

namespace Drupal\task_nested\Plugin\GraphQL\Schema;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistry;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\Schema\SdlSchemaPluginBase;
use Drupal\task_nested\Wrappers\TaskNestedQueryConnection;

/**
 * Provides the task schema.
 *
 * @Schema(
 *   id = "task_nested",
 *   name = "Task nested schema"
 * )
 */
final class TaskNestedSchema extends SdlSchemaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getResolverRegistry(): ResolverRegistryInterface {
    $builder = new ResolverBuilder();
    $registry = new ResolverRegistry();

    $this->addQueryFields($registry, $builder);
    $this->addTaskFields($registry, $builder);

    // Re-usable connection type fields.
    $this->addConnectionFields('TaskNestedConnection', $registry, $builder);

    return $registry;
  }

  /**
   * Adds resolvers for the task type.
   */
  private function addTaskFields(ResolverRegistry $registry, ResolverBuilder $builder): void {
    $registry->addFieldResolver('TaskNested', 'id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TaskNested', 'uuid',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TaskNested', 'title',
      $builder->compose(
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent()),
        $builder->produce('uppercase')
          ->map('string', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('TaskNested', 'author',
      $builder->compose(
        $builder->produce('entity_owner')
          ->map('entity', $builder->fromParent()),
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('TaskNested', 'published',
      $builder->compose(
        $builder->produce('entity_published')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('TaskNested', 'description',
      $builder->compose(
        $builder->produce('task_description')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('TaskNested', 'created',
      $builder->compose(
        $builder->produce('entity_created')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('TaskNested', 'changed',
      $builder->compose(
        $builder->produce('entity_changed')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('TaskNested', 'sub_tasks',
      $builder->produce('entity_reference')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('lines'))
    );

    $registry->addFieldResolver('TaskNestedItem', 'id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TaskNestedItem', 'uuid',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TaskNestedItem', 'title',
      $builder->produce('entity_label')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('TaskNestedItem', 'author',
      $builder->compose(
        $builder->produce('entity_owner')
          ->map('entity', $builder->fromParent()),
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('TaskNestedItem', 'published',
      $builder->compose(
        $builder->produce('entity_published')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('TaskNestedItem', 'description',
      $builder->compose(
        $builder->produce('task_description')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('TaskNestedItem', 'created',
      $builder->compose(
        $builder->produce('entity_created')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('TaskNestedItem', 'changed',
      $builder->compose(
        $builder->produce('entity_changed')
          ->map('entity', $builder->fromParent())
      )
    );
  }

  /**
   * Adds resolvers for the query type.
   */
  private function addQueryFields(ResolverRegistry $registry, ResolverBuilder $builder): void {
    $registry->addFieldResolver('Query', 'task_nested',
      $builder->produce('entity_load')
        ->map('type', $builder->fromValue('task_nested'))
        ->map('id', $builder->fromArgument('id'))
    );

    $registry->addFieldResolver('Query', 'tasks_nested',
      $builder->produce('query_task_nested')
        ->map('offset', $builder->fromArgument('offset'))
        ->map('limit', $builder->fromArgument('limit'))
    );
  }

  /**
   * Adds resolvers for the connection type.
   */
  private function addConnectionFields(string $type, ResolverRegistry $registry, ResolverBuilder $builder): void {
    $registry->addFieldResolver($type, 'total',
      $builder->callback(static function (TaskNestedQueryConnection $connection) {
        return $connection->total();
      })
    );

    $registry->addFieldResolver($type, 'items',
      $builder->callback(static function (TaskNestedQueryConnection $connection) {
        return $connection->items();
      })
    );
  }

}
