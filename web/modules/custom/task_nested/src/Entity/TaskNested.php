<?php

namespace Drupal\task_nested\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\task\Entity\TaskInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the task entity class.
 *
 * @ContentEntityType(
 *   id = "task_nested",
 *   label = @Translation("Task nested"),
 *   label_collection = @Translation("Tasks nested"),
 *   label_singular = @Translation("task nested"),
 *   label_plural = @Translation("tasks nested"),
 *   label_count = @PluralTranslation(
 *     singular = "@count tasks nested",
 *     plural = "@count tasks nested",
 *   ),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\task_nested\Access\TaskNestedAccessControlHandler",
 *     "storage" = "Drupal\task_nested\Storage\TaskNestedStorage",
 *     "route_provider" = {
 *       "html" = "Drupal\task_nested\Routing\TaskNestedHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "task_nested",
 *   revision_table = "task_nested_revision",
 *   admin_permission = "administer task",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   field_ui_base_route = "entity.task_nested.settings",
 * )
 */
final class TaskNested extends RevisionableContentEntityBase implements TaskNestedInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    if ($this->getOwnerId() === NULL) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setValues(array $values): void {
    foreach ($values as $key => $value) {
      $this->set($key, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getLines(): array {
    return $this->get('lines')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function setLines(array $entities): self {
    $this->set('lines', $entities);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addLine(TaskInterface $entity): self {
    $lines = $this->getLines();
    $lines[] = $entity;
    $this->setLines($lines);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return (int) $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields += self::ownerBaseFieldDefinitions($entity_type);
    $fields += self::publishedBaseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setRevisionable(TRUE)
      ->setLabel(t('Description'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['lines'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Task lines'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'task')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the task was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the task was last edited.'));

    return $fields;
  }

}
