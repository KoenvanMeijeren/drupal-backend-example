<?php

namespace Drupal\task_nested\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\task\Entity\TaskInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a task entity type.
 */
interface TaskNestedInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Sets the values for this entity.
   */
  public function setValues(array $values): void;

  /**
   * Get the task line entities.
   *
   * @return \Drupal\task\Entity\TaskInterface[]
   *   The task line entities.
   */
  public function getLines(): array;

  /**
   * Sets task line entities.
   *
   * @param \Drupal\task\Entity\TaskInterface[] $entities
   *   The task line entities.
   */
  public function setLines(array $entities): self;

  /**
   * Adds a task line to the task lines.
   */
  public function addLine(TaskInterface $entity): self;

  /**
   * Gets the description.
   */
  public function getDescription(): ?string;

  /**
   * Gets the created time.
   */
  public function getCreatedTime(): int;

}
