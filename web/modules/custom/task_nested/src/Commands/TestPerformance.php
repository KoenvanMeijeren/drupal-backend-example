<?php

namespace Drupal\task_nested\Commands;

use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drupal\task\Commands\TestPerformanceCommandBase;
use Drush\Attributes as CLI;
use Drush\SiteAlias\SiteAliasManagerAwareInterface;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * {@inheritDoc}
 */
final class TestPerformance extends TestPerformanceCommandBase implements SiteAliasManagerAwareInterface {

  use SiteAliasManagerAwareTrait;

  /**
   * Test the API's with various scenarios and show the results in the console.
   */
  #[CLI\Command(name: 'test:task_nested:performance')]
  #[CLI\Usage(name: 'drush test:task_nested:performance', description: 'Tests the performance of task nested endpoints.')]
  public function executePerformanceTest(): void {
    $this->execute([10, 100, 1000, 10_000], 5, 10, 'task_nested_execution_times.json');
  }

  /**
   * Test the API's with various scenarios and show the results in the console.
   */
  #[CLI\Command(name: 'test:task_nested:performance-results')]
  #[CLI\Usage(name: 'drush test:task_nested:performance-results', description: 'Shows the results of the performance tests.')]
  public function executeResultsDisplayBasedOnExistingFile(): void {
    $executionTimesFile = 'task_nested_execution_times.json';
    $executionTimes = (array) json_decode((string) file_get_contents($executionTimesFile), TRUE, 512, JSON_THROW_ON_ERROR);
    $this->executionTimes = $executionTimes;
    $output = $this->output();
    $this->displaySummaryTable($output);
    $this->displaySummarySummaryTable($output);
  }

  /**
   * {@inheritDoc}
   */
  protected function testApi(OutputInterface $output, int $maxRunsPerTest, int $sleepTime, array $entityCounts): void {
    $table = new Table($output);
    $table->setHeaders(['Entities', 'Scenario', 'API type', 'Run', 'Execution Time (ms)']);

    foreach ($entityCounts as $numEntities) {
      $this->seedDatabase($numEntities);
      $bearerToken = $this->getAccessToken();

      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task_nested/task_nested', $bearerToken, $maxRunsPerTest, 0, 'Cleared Cache', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task_nested/task_nested', $bearerToken, $maxRunsPerTest, $sleepTime, 'Cleared Cache waited', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task_nested/task_nested', $bearerToken, $maxRunsPerTest, 0, 'Normal', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task_nested/task_nested', $bearerToken, $maxRunsPerTest, $sleepTime, 'Normal waited', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task_nested/task_nested?page[limit]=50&page[offset]=0', $bearerToken, $maxRunsPerTest, 0, 'Paginated', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task_nested/task_nested?page[limit]=50&page[offset]=0', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated waited', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task_nested/task_nested?page[limit]=50&page[offset]=0', $bearerToken, $maxRunsPerTest, 0, 'Paginated Cleared Cache', $numEntities);
      $this->testScenario($table, $output, 'jsonApi', 'http://nginx/jsonapi/task_nested/task_nested?page[limit]=50&page[offset]=0', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated Cleared Cache waited', $numEntities);

      $this->testScenario($table, $output, 'custom-entity-manager', 'http://nginx/api/v1/tasks-nested', $bearerToken, $maxRunsPerTest, 0, 'Cleared Cache', $numEntities);
      $this->testScenario($table, $output, 'custom-entity-manager', 'http://nginx/api/v1/tasks-nested', $bearerToken, $maxRunsPerTest, $sleepTime, 'Cleared Cache waited', $numEntities);
      $this->testScenario($table, $output, 'custom-entity-manager', 'http://nginx/api/v1/tasks-nested', $bearerToken, $maxRunsPerTest, 0, 'Normal', $numEntities);
      $this->testScenario($table, $output, 'custom-entity-manager', 'http://nginx/api/v1/tasks-nested', $bearerToken, $maxRunsPerTest, $sleepTime, 'Normal waited', $numEntities);
      $this->testScenario($table, $output, 'custom-entity-manager', 'http://nginx/api/v1/tasks-nested?limit=50&page=1', $bearerToken, $maxRunsPerTest, 0, 'Paginated', $numEntities);
      $this->testScenario($table, $output, 'custom-entity-manager', 'http://nginx/api/v1/tasks-nested?limit=50&page=1', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated waited', $numEntities);
      $this->testScenario($table, $output, 'custom-entity-manager', 'http://nginx/api/v1/tasks-nested?limit=50&page=1', $bearerToken, $maxRunsPerTest, 0, 'Paginated Cleared Cache', $numEntities);
      $this->testScenario($table, $output, 'custom-entity-manager', 'http://nginx/api/v1/tasks-nested?limit=50&page=1', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated Cleared Cache waited', $numEntities);

      $this->testScenario($table, $output, 'custom-manually', 'http://nginx/api/v1/tasks-nested-manually', $bearerToken, $maxRunsPerTest, 0, 'Cleared Cache', $numEntities);
      $this->testScenario($table, $output, 'custom-manually', 'http://nginx/api/v1/tasks-nested-manually', $bearerToken, $maxRunsPerTest, $sleepTime, 'Cleared Cache waited', $numEntities);
      $this->testScenario($table, $output, 'custom-manually', 'http://nginx/api/v1/tasks-nested-manually', $bearerToken, $maxRunsPerTest, 0, 'Normal', $numEntities);
      $this->testScenario($table, $output, 'custom-manually', 'http://nginx/api/v1/tasks-nested-manually', $bearerToken, $maxRunsPerTest, $sleepTime, 'Normal waited', $numEntities);
      $this->testScenario($table, $output, 'custom-manually', 'http://nginx/api/v1/tasks-nested-manually?limit=50&page=1', $bearerToken, $maxRunsPerTest, 0, 'Paginated', $numEntities);
      $this->testScenario($table, $output, 'custom-manually', 'http://nginx/api/v1/tasks-nested-manually?limit=50&page=1', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated waited', $numEntities);
      $this->testScenario($table, $output, 'custom-manually', 'http://nginx/api/v1/tasks-nested-manually?limit=50&page=1', $bearerToken, $maxRunsPerTest, 0, 'Paginated Cleared Cache', $numEntities);
      $this->testScenario($table, $output, 'custom-manually', 'http://nginx/api/v1/tasks-nested-manually?limit=50&page=1', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated Cleared Cache waited', $numEntities);

      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task-nested', $bearerToken, $maxRunsPerTest, 0, 'Cleared Cache', $numEntities, '{"query":"query {\\n  tasks_nested(offset: 0, limit: -1) {\\n    total\\n    items {\\n      id\\n      title\\n      author\\n\\t\\t\\tsub_tasks {\\n        id\\n        title\\n        author\\n      }\\n    }\\n  }\\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task-nested', $bearerToken, $maxRunsPerTest, $sleepTime, 'Cleared Cache waited', $numEntities, '{"query":"query {\\n  tasks_nested(offset: 0, limit: -1) {\\n    total\\n    items {\\n      id\\n      title\\n      author\\n\\t\\t\\tsub_tasks {\\n        id\\n        title\\n        author\\n      }\\n    }\\n  }\\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task-nested', $bearerToken, $maxRunsPerTest, 0, 'Normal', $numEntities, '{"query":"query {\\n  tasks_nested(offset: 0, limit: -1) {\\n    total\\n    items {\\n      id\\n      title\\n      author\\n\\t\\t\\tsub_tasks {\\n        id\\n        title\\n        author\\n      }\\n    }\\n  }\\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task-nested', $bearerToken, $maxRunsPerTest, $sleepTime, 'Normal waited', $numEntities, '{"query":"query {\\n  tasks_nested(offset: 0, limit: -1) {\\n    total\\n    items {\\n      id\\n      title\\n      author\\n\\t\\t\\tsub_tasks {\\n        id\\n        title\\n        author\\n      }\\n    }\\n  }\\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task-nested', $bearerToken, $maxRunsPerTest, 0, 'Paginated', $numEntities, '{"query":"query {\\n  tasks_nested(offset: 0, limit: 50) {\\n    total\\n    items {\\n      id\\n      title\\n      author\\n\\t\\t\\tsub_tasks {\\n        id\\n        title\\n        author\\n      }\\n    }\\n  }\\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task-nested', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated waited', $numEntities, '{"query":"query {\\n  tasks_nested(offset: 0, limit: 50) {\\n    total\\n    items {\\n      id\\n      title\\n      author\\n\\t\\t\\tsub_tasks {\\n        id\\n        title\\n        author\\n      }\\n    }\\n  }\\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task-nested', $bearerToken, $maxRunsPerTest, 0, 'Paginated Cleared Cache', $numEntities, '{"query":"query {\\n  tasks_nested(offset: 0, limit: 50) {\\n    total\\n    items {\\n      id\\n      title\\n      author\\n\\t\\t\\tsub_tasks {\\n        id\\n        title\\n        author\\n      }\\n    }\\n  }\\n}"}');
      $this->testScenario($table, $output, 'graphql', 'http://nginx/graphql/task-nested', $bearerToken, $maxRunsPerTest, $sleepTime, 'Paginated Cleared Cache waited', $numEntities, '{"query":"query {\\n  tasks_nested(offset: 0, limit: 50) {\\n    total\\n    items {\\n      id\\n      title\\n      author\\n\\t\\t\\tsub_tasks {\\n        id\\n        title\\n        author\\n      }\\n    }\\n  }\\n}"}');
    }

    $output->writeln('');
    $output->writeln('<fg=blue>Results of the performance test of task endpoints...</>');
    $table->render();

    $output->writeln('');
    $output->writeln('<fg=blue>Summary of the performance of task endpoints...</>');
    $this->displaySummaryTable($output);

    $output->writeln('');
    $output->writeln('<fg=blue>Summary of the summary of the performance of task endpoints...</>');
    $this->displaySummarySummaryTable($output);
  }

  /**
   * {@inheritDoc}
   */
  protected function seedDatabase(int $amount, int $retry = 0): void {
    $self = $this->siteAliasManager()->getSelf();
    // @phpstan-ignore-next-line
    $process = $this->processManager()->drush($self, "task-nested:seed:database", ["amount" => $amount]);
    $process->mustRun($process->showRealtime());
  }

}
