<?php

namespace Drupal\task_nested\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\task\Storage\TaskStorageInterface;
use Drupal\task_nested\Storage\TaskNestedStorageInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Faker\Factory as FakerFactory;

/**
 * Provides a class for the update config command.
 *
 * @package Drupal\general\Command
 */
final class SeedDatabase extends DrushCommands {

  /**
   * The task storage.
   */
  private readonly TaskNestedStorageInterface $taskNestedStorage;

  /**
   * The task storage.
   */
  private readonly TaskStorageInterface $taskStorage;

  /**
   * Constructs a new object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly Connection $database,
  ) {
    parent::__construct();

    $this->taskStorage = $this->entityTypeManager->getStorage('task');
    $this->taskNestedStorage = $this->entityTypeManager->getStorage('task_nested');
  }

  /**
   * Updates the config.
   */
  #[CLI\Command(name: 'task-nested:seed:database')]
  #[CLI\Usage(name: 'drush task-nested:seed:database', description: 'Seeds the database with task nested entities.')]
  #[CLI\Argument(name: 'amount', description: 'The amount of task nested entities to seed the database with.')]
  public function execute(int $amount): void {
    $output = $this->output();
    $faker = FakerFactory::create();

    $output->writeln('<info>Seeding the database with task nested entities...</info>');

    $output->writeln('<info>Deleting all existing task nested entities...</info>');
    $this->database->truncate('task')->execute();
    $this->database->truncate('task_nested')->execute();
    $this->database->truncate('task_nested__lines')->execute();
    $this->database->truncate('task_revision')->execute();
    $this->database->truncate('task_nested_revision__lines')->execute();

    $tasks_amount = $amount < 1000 ? $amount * 5 : $amount;
    $output->writeln("<info>Creating {$tasks_amount} new task entities...</info>");
    $tasks = [];
    for ($index = 0; $index < $tasks_amount; $index++) {
      $task = $this->taskStorage->create([
        'label' => $faker->words(3, TRUE),
        'description' => $faker->paragraph(10),
        'status' => TRUE,
      ]);

      $task->save();

      $tasks[] = $task;
    }

    $output->writeln("<info>Creating {$amount} new task nested entities...</info>");
    for ($task_index = 0; $task_index < $amount; $task_index++) {
      $randomTasks = array_rand($tasks, 5);

      $this->taskNestedStorage->create([
        'label' => "Task nested {$task_index}",
        'description' => $faker->paragraph(10),
        'lines' => $randomTasks,
        'status' => TRUE,
      ])->save();
    }

    $output->writeln('<info>Database seeded with task nested entities.</info>');
  }

  /**
   * Updates the config.
   */
  #[CLI\Command(name: 'task-nested:insert:database')]
  #[CLI\Usage(name: 'drush task-nested:insert:database', description: 'Seeds the database with task nested entities.')]
  #[CLI\Argument(name: 'amount', description: 'The amount of task nested entities to seed the database with.')]
  public function executeInsert(int $amount): void {
    $output = $this->output();
    $faker = FakerFactory::create();

    $tasks_amount = $amount < 1000 ? $amount * 5 : $amount;
    $output->writeln("<info>Adding {$tasks_amount} new task entities...</info>");
    $tasks = [];
    for ($index = 0; $index < $tasks_amount; $index++) {
      $task = $this->taskStorage->create([
        'label' => $faker->words(3, TRUE),
        'description' => $faker->paragraph(10),
        'status' => TRUE,
      ]);

      $task->save();

      $tasks[] = $task;
    }

    $output->writeln("<info>Creating {$amount} new task nested entities...</info>");
    for ($task_index = 0; $task_index < $amount; $task_index++) {
      $randomTasks = array_rand($tasks, 5);

      $this->taskNestedStorage->create([
        'label' => "Task nested {$task_index}",
        'description' => $faker->paragraph(10),
        'lines' => $randomTasks,
        'status' => TRUE,
      ])->save();
    }

    $output->writeln('<info>Database updated with task nested entities.</info>');
  }

}
