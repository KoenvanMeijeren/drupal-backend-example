<?php

namespace Drupal\task_nested\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\task_nested\Entity\TaskNestedInterface;

/**
 * Defines the storage handler class for Task entities.
 *
 * This extends the base storage class, adding required special handling for
 * Task entities.
 *
 * @ingroup task
 */
final class TaskNestedStorage extends SqlContentEntityStorage implements TaskNestedStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByUuid(string $uuid): ?TaskNestedInterface {
    $entities = $this->loadByProperties(['uuid' => $uuid]);
    if ($entities === []) {
      return NULL;
    }

    // @phpstan-ignore-next-line
    return reset($entities) ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadPaginated(int $page, int $limit): array {
    $query = $this->getQuery();
    $query->pager($limit, $page);
    $query->sort('created', 'DESC');
    $query->accessCheck();

    return $this->loadMultiple($query->execute());
  }

}
