<?php

namespace Drupal\task_nested\Storage;

use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\task_nested\Entity\TaskNestedInterface;

/**
 * Defines the storage handler class for Task entities.
 *
 * This extends the base storage class, adding required special handling for
 * Task entities.
 *
 * @ingroup task
 */
interface TaskNestedStorageInterface extends SqlEntityStorageInterface {

  /**
   * Loads a task by its UUID.
   */
  public function loadByUuid(string $uuid): ?TaskNestedInterface;

  /**
   * Loads tasks with pagination.
   *
   * @return \Drupal\task_nested\Entity\TaskNestedInterface[]
   *   The tasks.
   */
  public function loadPaginated(int $page, int $limit): array;

}
