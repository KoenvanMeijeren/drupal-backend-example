<?php

// @codingStandardsIgnoreFile

$databases = [];
$databases['default']['default'] = [
  'database' => getenv('MYSQL_DATABASE'),
  'username' => getenv('MYSQL_USER'),
  'password' => getenv('MYSQL_PASSWORD'),
  'driver' => 'mysql',
  'prefix' => '',
  'host' => getenv('MYSQL_HOSTNAME'),
  'port' => getenv('MYSQL_PORT'),
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'init_commands' => [
    'isolation_level' => 'SET SESSION tx_isolation=\'READ-COMMITTED\'',
  ],
];

$settings['config_sync_directory'] = '../config/sync';

$settings['hash_salt'] = 'JCbSuFTgNuS0Hu-b0p59_5PnE81_x8AqY3mnAfFkAciexQZ5DM0XWay9iJGQr6iRA4g5T4Qfow';
$settings['update_free_access'] = FALSE;

$settings['file_private_path'] = '../private_files';

$settings['container_yamls'][] = __DIR__ . '/services.yml';

$settings['trusted_host_patterns'] = [
  '^localhost$',
  'nginx',
];

$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

$settings['entity_update_batch_size'] = 50;


// Set the max rows to infinite.
$settings['database_cache_max_rows']['default'] = -1;

// The render cache shoud NEVER be in redis for performance reasons.
// Same goes for the database.
$settings['cache']['bins']['render'] = 'cache.backend.php';
$settings['cache']['bins']['page'] = 'cache.backend.php';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.php';

/*
 Locally, it's faster to have 'config' on 'chainedfast',
 but on staging and production the 'php' backend is way faster.
*/
$settings['cache']['bins']['config'] = 'cache.backend.php';

// Save static cache only statically.
$settings['cache']['bins']['static'] = 'cache.backend.memory';

if (isset($GLOBALS['install_state'])) {
  $config['automated_cron.settings']['interval'] = 0;
}

if (file_exists(__DIR__ . '/settings.local.php')) {
  include __DIR__ . '/settings.local.php';
}

// Include environment specific configuration.
$environment = $_SERVER['ENVIRONMENT'] ?? 'development';
if (file_exists(__DIR__ . '/settings.' . $environment . '.php')) {
  include __DIR__ . '/settings.' . $environment . '.php';
}

if (file_exists(__DIR__ . '/services.local.yml')) {
  $settings['container_yamls'][] = __DIR__ . '/services.local.yml';
}

$databases['default']['default']['init_commands']['optimizer_search_depth'] = 'SET SESSION optimizer_search_depth = 7';
