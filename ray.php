<?php

/**
 * @file
 * The ray.php file.
 */

declare(strict_types=1);

return [
  'enable' => TRUE,
  'host' => 'host.docker.internal',
  'port' => 23517,
  'remote_path' => '/drupal-backend-example/web',
  'always_send_raw_values' => FALSE,
];
