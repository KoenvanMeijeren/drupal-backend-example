<?php

namespace Tests\Unit\Commands;

use Drupal\Core\Config\Config;
use Drupal\general\Commands\UpdateAfterInstallation;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the UpdateAfterInstallation command.
 *
 * @group general
 */
final class UpdateAfterInstallationTest extends UnitTestCase {

  /**
   * The command to be tested.
   *
   * @var UpdateAfterInstallation
   */
  protected $command;

  /**
   * The config factory mock.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Mock the ConfigFactoryInterface.
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);

    // Instantiate the command with the mock ConfigFactory.
    $this->command = new UpdateAfterInstallation($this->configFactory);
  }

  /**
   * Tests the update-after-install command.
   */
  public function testUpdateAfterInstall(): void {
    // Arrange.
    $configMock = $this->getConfigMock();
    $this->configFactory->expects(self::once())
      ->method('getEditable')
      ->with('system.performance')
      ->willReturn($configMock);

    $configMock->expects(self::exactly(2))
      ->method('set')
      ->willReturnCallback(fn (string $property, bool $value) => match ($property) {
        'css.preprocess', 'js.preprocess' => FALSE,
        default => self::fail(sprintf('Unexpected property "%s" passed to set().', $property)),
      });
    $configMock->expects(self::once())
      ->method('save');

    // Act.
    $this->command->execute();
  }

  /**
   * Helper method to mock the Config object.
   */
  private function getConfigMock(): MockObject {
    return $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->getMock();
  }

}
