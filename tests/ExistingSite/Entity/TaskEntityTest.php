<?php

namespace Tests\ExistingSite\Entity;

use Drupal\task\Entity\Task;
use weitzman\DrupalTestTraits\ExistingSiteBase;

/**
 * Provides a class for TaskEntityTest.
 */
final class TaskEntityTest extends ExistingSiteBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Cause tests to fail if an error is sent to Drupal logs.
    $this->failOnLoggedErrors();
  }

  /**
   * Tests the creation of the task entity.
   */
  public function testEntityCreate(): void {
    // Create a task entity.
    $task = Task::create([
      'label' => 'Test task',
      'description' => 'Test task description',
      'status' => TRUE,
    ]);

    // Assert that the task entity is created.
    self::assertEquals(0, $task->getOwnerId());
    self::assertEquals('Test task', $task->label());
    self::assertEquals('Test task description', $task->getDescription());
    self::assertTrue($task->isPublished());
  }

  /**
   * Tests the creation of the task entity.
   */
  public function testEntitySave(): void {
    // Create a task entity.
    $task = Task::create([
      'label' => 'Test task',
      'description' => 'Test task description',
      'status' => TRUE,
    ]);

    // Save the task entity.
    $task->save();

    // Assert that the task entity is created.
    self::assertEquals('Test task', $task->label());
    self::assertEquals('Test task description', $task->getDescription());
    self::assertTrue($task->isPublished());
  }

}
