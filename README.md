# Drupal Backend Test project

## Installation

The installation of the site can be done via the script below:

```bash
sh ./scripts/install.sh
```

## Run the app

Execute this command to run the app:

```bash
docker compose -f docker-compose.yml -f docker-compose.dev.yml up -d
docker compose -f docker-compose.yml -f docker-compose.dev.yml exec php /bin/bash
```

## Performance testing

To test the performance of the app, follow the steps below:

1. Install the app
2. Run the app
3. Open Insomnia or Postman
4. Run the tests manually per endpoint

### Automated performance testing

To run the automated performance testing, run the following commands:

```bash
drush test:task:performance
drush test:task_nested:performance
```

## Results

### Reading 10 entities

![img_1.png](assets/img_1.png)

### Reading 100 entities

![img.png](assets/img.png)

### Reading 1000 entities

![img_2.png](assets/img_2.png)

### Reading 10000 entities

![img_3.png](assets/img_3.png)

## Results with GraphQL Extra Cache

### With module enabled

![img_6.png](assets/img_6.png)

### Without module enabled

![img_7.png](assets/img_7.png)

### Conclusion

Positive differences indicate that the execution time was faster with cache, and negative differences indicate it was faster without cache.

| Entities | Scenario                       | No Cache (ms) | With Cache (ms) | Difference (ms) |
|----------|--------------------------------|---------------|-----------------|-----------------|
| 10       | Cleared Cache                  | 521.29        | 450.35          | 70.94           |
| 10       | Cleared Cache waited           | 496.02        | 461.90          | 34.12           |
| 10       | Normal                         | 32.22         | 23.51           | 8.71            |
| 10       | Normal waited                  | 251.39        | 222.87          | 28.52           |
| 10       | Paginated                      | 28.16         | 28.79           | -0.63           |
| 10       | Paginated waited               | 238.93        | 249.38          | -10.45          |
| 10       | Paginated Cleared Cache        | 443.22        | 499.09          | -55.87          |
| 10       | Paginated Cleared Cache waited | 427.27        | 487.77          | -60.50          |
| 100      | Cleared Cache                  | 584.00        | 496.06          | 87.94           |
| 100      | Cleared Cache waited           | 583.61        | 599.50          | -15.89          |
| 100      | Normal                         | 29.30         | 25.42           | 3.88            |
| 100      | Normal waited                  | 262.42        | 225.57          | 36.85           |
| 100      | Paginated                      | 35.81         | 34.03           | 1.78            |
| 100      | Paginated waited               | 249.17        | 228.05          | 21.12           |
| 100      | Paginated Cleared Cache        | 459.35        | 519.99          | -60.64          |
| 100      | Paginated Cleared Cache waited | 464.00        | 571.23          | -107.23         |
| 1,000    | Cleared Cache                  | 1,553.16      | 1,389.64        | 163.52          |
| 1,000    | Cleared Cache waited           | 1,373.66      | 1,433.30        | -59.64          |
| 1,000    | Normal                         | 37.02         | 40.86           | -3.84           |
| 1,000    | Normal waited                  | 254.16        | 243.79          | 10.37           |
| 1,000    | Paginated                      | 32.58         | 38.94           | -6.36           |
| 1,000    | Paginated waited               | 265.17        | 220.30          | 44.87           |
| 1,000    | Paginated Cleared Cache        | 456.48        | 485.95          | -29.47          |
| 1,000    | Paginated Cleared Cache waited | 493.01        | 511.63          | -18.62          |

The analysis of the average execution times for various scenarios with and without cache reveals the following insights:

1. Performance Improvement with Cache:
    1. In most scenarios, the presence of a cache significantly improves performance. For example, with 1,000 entities in a cleared cache scenario, the average execution time decreased from 1,553.16 ms to 1,389.64 ms, resulting in a 163.52 ms improvement.
    2. The "Normal" and "Normal waited" scenarios consistently show better performance with cache, highlighting the efficiency of cached responses for standard requests. For instance, the execution time for "Normal waited" with 100 entities improved by 36.85 ms.

2. Mixed Results in Paginated Scenarios:
    1. The paginated scenarios demonstrate mixed results. While some show improvement, others indicate slower performance with cache. For example, the "Paginated" scenario with 10 entities shows a slight increase in execution time by 0.63 ms with cache, while the "Paginated waited" scenario with 1,000 entities improves by 44.87 ms with cache.

3. Cleared Cache Scenarios:
    1. The cleared cache scenarios generally benefit from cache, especially as the number of entities increases. However, there are exceptions, such as the "Cleared Cache waited" scenario with 1,000 entities, which saw a slight increase in execution time by 59.64 ms.

4. Anomalies:
   1. Certain scenarios like "Paginated Cleared Cache" and "Paginated Cleared Cache waited" with 10 and 100 entities show unexpected increases in execution times with cache, suggesting potential inefficiencies or overheads introduced by caching mechanisms in these specific contexts.

Overall, caching typically enhances performance, particularly in non-paginated and high-entity scenarios. However, the observed anomalies in some paginated scenarios suggest the need for further investigation to optimize caching strategies and ensure consistent performance improvements across all use cases.
