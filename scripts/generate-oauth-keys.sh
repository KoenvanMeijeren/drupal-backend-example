#!/usr/bin/env bash
storage_dir="web/oauth-keys"

mkdir -p "$storage_dir"

# Generate private key
openssl genrsa -out "$storage_dir/private.key" 2048

# Generate public key from private key
openssl rsa -in "$storage_dir/private.key" -pubout -out "$storage_dir/public.key"

chmod 600 "$storage_dir/public.key"
chmod 600 "$storage_dir/private.key"
