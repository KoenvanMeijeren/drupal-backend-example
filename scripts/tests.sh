#!/usr/bin/env bash
set -x

# Run phpcs and save the result
composer run phpcs
PHPCS_RESULT=$?

# Run phpstan and save the result
composer run phpstan
PHPSTAN_RESULT=$?

# Run phpmd and save the result
composer run phpmd
PHPMD_RESULT=$?

# Run phpunit and save the result
vendor/bin/phpunit
PHPUNIT_RESULT=$?

# If all tests passed, exit with 0, otherwise exit with 1
if [ $PHPCS_RESULT -eq 0 ] && [ $PHPSTAN_RESULT -eq 0 ] && [ $PHPMD_RESULT -eq 0 ] && [ $PHPUNIT_RESULT -eq 0 ]; then
    echo "All tests passed successfully."
    exit 0
fi

echo "Some tests failed. Check the output for details."
exit 1
