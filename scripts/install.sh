#!/usr/bin/env bash
set -x -e

sh ./scripts/generate-oauth-keys.sh

vendor/bin/drush sql-drop -y
vendor/bin/drush si drupal_backend_example --existing-config --account-pass="nimda" --account-mail="info@site.com" --site-mail="info@site.com" --site-name="Demo" -y --verbose
vendor/bin/drush update-after-install

# Allow chmod to fail without stopping the script
chmod u+w web/sites/default || true

vendor/bin/drush cr

vendor/bin/drush en site_default_content -y
vendor/bin/drush pmu site_default_content -y

vendor/bin/drush cr
